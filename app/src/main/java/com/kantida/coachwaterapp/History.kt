package com.kantida.coachwaterapp

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ListView
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.kantida.coachwaterapp.model.Water

class History : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_history)

        val oilList: List<Water> = listOf(
            Water( "08.30 AM","200 ml"),
            Water("09.40 AM", "100 ml"),
            Water("10.30 AM", "200 ml"),
            Water("11.40 AM", "400 ml"),
            Water("13.20 PM", "100 ml"),
            Water("14.10 PM", "200 ml"),
            Water("15.30 PM", "100 ml"),
            Water("14.40 PM", "100 ml"),

        )
        val recyclerView = findViewById<RecyclerView>(R.id.recycler_view)
        recyclerView.layoutManager = LinearLayoutManager(this)
        recyclerView.adapter = ItemAdapter(oilList)

    }
    class ItemAdapter(val oilList: List<Water>) : RecyclerView.Adapter<ItemAdapter.ViewHolder>() {
        class ViewHolder(private val itemView: View) : RecyclerView.ViewHolder(itemView) {
            val time = itemView.findViewById<TextView>(R.id.txt_view)
            val oz = itemView.findViewById<TextView>(R.id.txt_view2)
        }

        override fun onBindViewHolder(holder: ViewHolder, position: Int) {
//
            holder.time.text = oilList[position].time.toString()
            holder.oz.text = oilList[position].oz
//
//            holder.itemView.setOnClickListener(View.OnClickListener {
//                Toast.makeText(
//                    holder.itemView.getContext(),
//                    "ชื่อน้ำมัน : " + holder.name.text + " ราคา : " + holder.price.text + " บาท/ลิตร",
//                    Toast.LENGTH_LONG
//                ).show()
//            })
        }

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemAdapter.ViewHolder {
            val adapterLayout =
                LayoutInflater.from(parent.context).inflate(R.layout.water_item, parent, false)
            return ViewHolder(adapterLayout)
        }

        override fun getItemCount(): Int {
            return oilList.size
        }

    }

    fun goNextPage(v: View){
        val intent = Intent(this@History, Home::class.java)
        startActivity(intent)
    }



}