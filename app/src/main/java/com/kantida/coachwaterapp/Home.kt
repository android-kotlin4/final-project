package com.kantida.coachwaterapp

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.ListView
import kotlinx.android.synthetic.main.activity_home.*

class Home : AppCompatActivity(), View.OnClickListener {
    private var progressBar = 0
    private var oz = 0
    val water = arrayOf("10.03 AM","11.30 AM","13.35 PM","14.25 PM","15.03 PM","16 PM")

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)

        updateProgressBar()
        btnAddWater1.setOnClickListener(this)
        btnAddWater2.setOnClickListener(this)
        btnAddWater3.setOnClickListener(this)
        btnAddWater4.setOnClickListener(this)

//        val listview = findViewById<ListView>(R.id.list)
//        val adapter = ArrayAdapter(this, android.R.layout.simple_list_item_1, water)
////        listview.adapter = adapter
//        listview.onItemClickListener = object : AdapterView.OnItemClickListener{
//            override fun onItemClick(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {

//            }
//
//        }

    }

    override fun onClick(v: View?) {
        when (v) {
            btnAddWater1 -> {
                if (progressBar <= 95) {
                    progressBar += 5
                    oz += 100
                    updateProgressBar()
                }
            }
            btnAddWater2 -> {
                if (progressBar <= 95) {
                    progressBar += 10
                    oz += 300
                    updateProgressBar()
                }
            }
            btnAddWater3 -> {
                if (progressBar <= 95) {
                    progressBar += 15
                    oz += 500
                    updateProgressBar()
                }
            }
            btnAddWater4 -> {
                if (progressBar <= 95) {
                    progressBar += 20
                    oz += 1000
                    updateProgressBar()
                }
            }

        }
    }

    private fun updateProgressBar() {
        progress_bar.progress = progressBar
        txtProgressBar.text = progressBar.toString()
        txtshowVolume.text = oz.toString()
    }

    fun goNextPage(v: View){
        val intent = Intent(this@Home, History::class.java)
        startActivity(intent)
    }
}
